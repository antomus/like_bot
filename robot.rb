require 'watir-webdriver'
require_relative 'pages/init'

class Steps
  def login_user
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.timeout = 360 # seconds – default is 60
    browser = Watir::Browser.new :firefox, :http_client => client
    login_page = LoginPage.new(browser)
    login_page.open
    login_page.open_login_popup
    login_page.login
    browser
  end

  def run
    browser = login_user
    teams_page = TeamsPage.new(browser)
    teams_page.open_thread
    teams_page.add_to_favorites
  end
end

Steps.new.run