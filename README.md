To run the bot please take next steps:

1. Install Ruby if not installed
2. Install Firefox if not installed
3. Install Git if not installed
4. Clone repository
5. Open terminal
6. Navigate to root folder of the project
7. Issue 'gem install bundler' if gem 'bundler' is not installed
8. Issue 'bundle install' in the terminal 
9. Issue 'ruby robot.rb' in the terminal

===========================================

Basic configuration can be made in config.yml