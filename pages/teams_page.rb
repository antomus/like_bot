class TeamsPage < BrowserContainer

  def open
    @browser.goto "#{@config['url']}/teams"
    self
  end

  def open_thread
    open_discussions
    search_thread
    threads_link.click
    Watir::Wait.until { @browser.title.include?(@config['thread']) }
  end

  def open_team
    open
    search_field.wait_until_present
    search_field.set @config['team']
    search_button.click
    team_link.wait_until_present
    team_link.click
    Watir::Wait.until { @browser.title.include?(@config['team']) }
  end

  def open_discussions
    open_team
    url = @browser.url << "/discuss"
    @browser.goto url
    Watir::Wait.until { @browser.text.include?('Discussions') }
  end

  def search_thread
    search_field.set @config['thread']
    search_button.click
    Watir::Wait.until { @browser.text.include?('Search results') }
  end

  def add_to_favorites
    count_pages = lambda{ paging_numbers.last.link.text.to_i }

    pg_count = count_pages.call
    favorites_count = 0
    real_pages_count = 0
    i = @config['position'] || 1

    post_link if pg_count  > 0 && @config['link_posted'] != Time.now.strftime("%Y-%m-%d")
    buttons_on_page = 0
    loop do
      break if i == pg_count
      url = @browser.url.split('/')[0..-3].join("/") << "/page/#{i}"
      
      sure_load(60) {
        # puts "i #{i}"
        # puts "url #{url}"
        @browser.goto url
      }

      Watir::Wait.until { @browser.text.include? 'Responses' }

      buttons_on_page = @browser.links(:class, 'button-fave')
      bt_size = buttons_on_page.size
      real_pages_count += 1 if bt_size > 0
      favorites_count += bt_size
      bt_size.times do |j|

        if buttons_on_page[j].class_name.split(' ').include?('favorited-button')
          2.times do
            buttons_on_page[j].click
            sleep rand(4..7)
          end
        else
          buttons_on_page[j].click
          sleep rand(10..16)
        end
      end

      pg_count = count_pages.call
      i += 1
    end

    post_comment if pg_count > 0
    set_position_after_favoriting(i + 1) 

    STDERR.puts "Pages count: #{real_pages_count}"
    STDERR.puts "Favorites count: #{favorites_count}"
  end

  def post_txt(txt)
    post_area.set txt
    post_submit_buttom.click
  end

  def post_link
    post_txt @config['link']
  end

  def post_comment
    post_txt @config['comment']
  end

  def set_position_after_favoriting(page_num)
    @config['position'] = page_num
    if !@config['link_posted'] || (@config['link_posted'] != Time.now.strftime("%Y-%m-%d"))
      @config['link_posted'] = Time.now.strftime("%Y-%m-%d")
    end
    File.open(CONFIG_PATH, 'w') {|f| f.write @config.to_yaml }
    reload_config!
  end


  private

  def search_field
    @browser.div(id: "listings-header").text_field(name: "search_query")
  end

  def search_button
    @browser.div(id: "listings-header").button(value: "Search")
  end

  def team_link
    @browser.link(text: @config['team'])
  end

  def threads_link
    @browser.ul(:class, 'threads').a(:text, /#{@config['thread'].upcase}/)
  end

  def paging_numbers
    @browser.div(:id, 'pager-wrapper').div(:class,'pager')
      .div(:class, 'pages').spans(:class, 'page_num')
  end

  def post_area
    @browser.form(:id, 'new-post-form').textarea(:name, 'post')
  end

  def post_submit_buttom
    @browser.input(:id, 'new-post-submit')
  end

end #TeamsPage 