  class LoginPage < BrowserContainer

    def open
      sure_load(60) {
        @browser.goto @config["url"]
      }
      self
    end

    def login
      user_field.wait_until_present
      user_field.set @config["username"]
      password_field.set @config["password"]
      login_button.click
      Watir::Wait.until { @browser.title == "Etsy :: Your place to buy and sell all things handmade" }
    end

    def open_login_popup
      sign_in_button.click
    end

    private

    def user_field
      @browser.text_field(:id => "username-existing")
    end

    def password_field
      @browser.text_field(:id => "password-existing")
    end

    def sign_in_button
      @browser.link(:id => "sign-in")
    end

    def login_button
      @browser.button(:id => "signin-button")
    end
  end # LoginPage