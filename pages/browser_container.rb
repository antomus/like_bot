require 'yaml'
class BrowserContainer

  CONFIG_PATH = File.dirname(__FILE__)+'/../config.yml'

  def initialize(browser)
    load_config
    @browser = browser
  end

  def close
    @browser.close if @browser
  end

  def load_config
    @config ||= YAML.load_file(CONFIG_PATH)
  end

  def reload_config!
    @config = nil
    load_config
  end

  def sure_load(mytimeout) 
    begin
      Timeout::timeout(mytimeout)  do
      yield
    end
    rescue Timeout::Error => e
      puts "Page load timed out: #{e}"
      retry
    end
  end
end